namespace Canine.Net.Client.Resources
{
    using Canine.Net.Infrastructure.Console;
    using Canine.Net.Infrastructure.Command;
    using Monosoft.Zip2City.Domain.Interfaces;

    internal class ZipCodeResource : BaseResource
    {
        public ZipCodeResource(IZipCodeRepository Repository) : base("ZipCode")
        {
            this.AddCommand(new Monosoft.Zip2City.Application.Resources.ZipCode.Commands.Get.Command(Repository), OperationType.Get, "Get", "Placeholder for a description");//TODO Claims and OperationType
        }
    }
}
