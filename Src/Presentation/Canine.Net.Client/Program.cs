using System.Linq;
using Canine.Net.Infrastructure.Console;
using Microsoft.Extensions.DependencyInjection;
using static Canine.Net.Infrastructure.Console.Program;
using Canine.Net.Infrastructure.Config;
using Monosoft.Zip2City.Persistence.PostgreSQL;
using Canine.Net.Client;

namespace Canine.Net.Clienta
{
    public static class Program
    {
        public static void Main()
        {
            var pwd = System.Environment.GetEnvironmentVariable("zip2city_sqlserver_password");
            var usr = System.Environment.GetEnvironmentVariable("zip2city_sqlserver_user");
            var databasename = System.Environment.GetEnvironmentVariable("zip2city_sqlserver_databasename");
            var port = System.Environment.GetEnvironmentVariable("zip2city_sqlserver_port");
            var servername = System.Environment.GetEnvironmentVariable("zip2city_sqlserver_name");

            var rabbitMQUsername = System.Environment.GetEnvironmentVariable("rabbitmq_username");
            var rabbitMQPassword = System.Environment.GetEnvironmentVariable("rabbitmq_password");
            var rabbitMQHost = System.Environment.GetEnvironmentVariable("rabbitmq_host");
            var rabbitMQPort = System.Environment.GetEnvironmentVariable("rabbitmq_port");

            RabbitMQSettings rabbitSet = new RabbitMQSettings(rabbitMQUsername, rabbitMQPassword, rabbitMQHost, rabbitMQPort != null ? int.Parse(rabbitMQPort) : 5672);

            ServiceProvider serviceProvider = new ServiceCollection().AddPostgreSQLPersistenceProvider(servername, usr, pwd, int.Parse(port), databasename).AddResources().BuildServiceProvider();
            StartRabbitMq("Zip2City",new ProgramVersion(1), serviceProvider.GetServices<IResource>().ToList(), rabbitSet);
        }
    }
}